#!/usr/bin/env python
#
#Made by unpluggedlink
#

import getpass
import os
import fnmatch


#Fuctions

def moveGroup(file, category):
    try:
        os.rename(file, home +'/'+ category +'/'+file)
    except FileNotFoundError:
        print("The directory: "+ home +'/'+ category +'/ was not found')
        home_files = os.listdir(home)
        counter = 0
        for file in home_files:
            if os.path.isdir(home +'/'+file):
                counter = counter + 1
                print(str(counter)+") "+file)
                selection = input("\nWrite the number of your "+category+" folder: ")
                os.rename(file, home +'/'+ home_files[int(selection)] +'/'+file)

def matchwcard(file, format):
    return fnmatch.fnmatch(file, "*"+format)

def printlist(printinglist):
    for item in printinglist:
        print(item)

def ls(path):
    """Create a list of the files on the path dir and returns the list"""
    file_list = os.listdir(path)
    return file_list

def separator(file_list):
    """Separates files by type ands move them to especific folder"""
    filtered_list = ["Music", [], "Videos", [], "Pictures", [], "Folders(this are not moved)", []]
    for file in file_list:
        if matchwcard(file, ".mp3") or matchwcard(file, ".ogg") or matchwcard(file, ".opus"):
            filtered_list[1].append(file)
            os.rename(file, home+'/'+filtered_list[0]+'/'+file) #in this line is were we especify a folder for the first time
        if matchwcard(file, ".mp4") or matchwcard(file,".mkv"):
            filtered_list[3].append(file)
            moveGroup(file, filtered_list[2])
        if matchwcard(file, ".jpg") or matchwcard(file, ".png") or matchwcard(file, ".jpeg") or matchwcard(file, ".gif"):
            os.rename(file, home+'/'+filtered_list[4]+'/'+file)
            filtered_list[5].append(file)
        if os.path.isdir(file):                                 #This checks and ignore folders (for now)        
            filtered_list[7].append(file)
    return filtered_list

#Variables
pwd = os.getcwd()
home = os.environ['HOME']
cwd_file_list = ls(pwd)
new_list = separator(cwd_file_list)
#The beggining of the program 
print('====+[Files in the current working directory]+====')
printlist(cwd_file_list)
print('====+[Files moved to /home/'+getpass.getuser()+"]+====")
printlist(new_list)
